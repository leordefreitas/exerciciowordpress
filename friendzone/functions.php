<?php 

// funcao do estilos do site
function brafe_scripts() {
  wp_enqueue_style( "style-sheet", get_stylesheet_directory_uri() . "/style.css" );
  wp_enqueue_style("reset-sheet", get_stylesheet_directory_uri() . "/reset.css");
}
add_action( 'wp_enqueue_scripts', 'brafe_scripts' );

// funcao do produtos
function custom_post_type() {
  $labels = array(
    'name' => _x('Produtos', 'post type general name'),
    'singular_name' => _x('Produto', 'post type singular name'),
    'add_new' => _x('Adicionar Novo', 'Novo item'),
    'add_new_item' => __('Novo Item'),
    'edit_item' => __('Editar Item'),
    'new_item' => __('Novo Item'),
    'view_item' => __('Ver Item'),
    'search_items' => __('Procurar Itens'),
    'not_found' =>  __('Nenhum registro encontrado'),
    'not_found_in_trash' => __('Nenhum registro encontrado na lixeira'),
    'parent_item_colon' => '',
    'menu_name' => 'Produtos'
  );

  $args = array(
    'label' => 'Produto',
    'description' => 'Produtos',
    'menu_position' => 3,
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'map_meta_cap' => true,
    'labels' => $labels,
    'public_queryable' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'has_archive' => true,
    'hierarchical' => false,
    'supports' => array('title','editor')
  );
  register_post_type( 'produto' , $args );
}
add_action('init', 'custom_post_type');
