<?php
    // Template Name: Notícias
?>

<?php get_header();
$paged = get_query_var('paged', 1);
$news = new WP_Query(
    array(
        'posts_per_page'    => 2,
        'paged'             => $paged,
        'post_type'         => 'noticia',
        'post_status'       => 'publish',
        'suppress_filters'  => true,
        'orderby'           => 'post_date', 
        'order'             => 'ASC'
    )
);
?>
<main>
    <h1 class ="stylenoticia">Notícias</h1>
    <div class= "stylesearch"> 
        <h3>Procurar Notícias</h3> 
        <form role="search" action="<?php echo site_url( '/' ); ?>" method="get" id ="searchform">
            <input type="text" name="s" placeholder="Pesquisar Notícia"/>
            <input type="hidden" name= "post_type" value="noticia"/>
            <input type="submit" alt="Search" value="Procurar"/>
        </form>
    </div>

    <div id = "noticia_container">
        <?php if ($news -> have_posts()): 
            while ($news -> have_posts()):
                $news -> the_post(); ?>
                <div class ="style_noticiabox card">
                    <h2 class = "style_titlenoticia"><?php the_title(); ?></h2>
                    <a class = "stylelink" href = "<?php the_permalink(); ?>">Link para a notícia</a>
                    <?php  the_content(); ?>
                    <div class = "styledate">
                        <p>Criado em: <?php echo get_the_date()?> </p>
                        <p>Editado em: <?php echo get_the_modified_date()?> </p>
                    </div>
                </div>
            <?php endwhile; 
        else:?>
            <p>Não há notícias!</p>
        <?php endif;  ?>
    </div>
    <div class = "style_pag">
        <?php
            $big = 99999999;
            echo paginate_links( array(
                'base'      => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
                'format'    => '?paged=%#%',
                'current'   => max( 1, get_query_var( 'paged' ) ),
                'prev_text' => __( 'Anterior' ),
                'next_text' => __( 'Próximo' ),
                'total'     => $news->max_num_pages)
            );
        ?>
    </div>
    <?php wp_reset_postdata(); ?>
</main>
<?php get_footer(); ?> 
