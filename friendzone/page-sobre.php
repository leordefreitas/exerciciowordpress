<?php
  // Template Name: Sobre
?>

<?php get_header(); ?>

<section id="sobre">
    <div id="imagen-texto-sobre">
      <img src="https://i.pinimg.com/originals/de/f6/96/def69643889ee29e232637646e839064.jpg" alt="">
      <div id="texto-links">
        <h2>Titulo</h2>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Labore voluptate fuga quae.</p>
        <ul id="rede-sociais">
          <li><a href="">Facebook</a></li>
          <li><a href="">Twitter</a></li>
          <li><a href="">Instagram</a></li>
        </ul>
      </div>
    </div>
  </section>

<?php get_footer(); ?>

