<?php 

//Template Post Type: noticia

?>

<?php get_header(); 
    $news = new WP_Query(
        array(
            'posts_per_page'    => 3,
            'paged'             => $paged,
            'post_type'         => 'noticia',
            'post_status'       => 'publish',
            'suppress_filters'  => true,
            'orderby'           => 'rand', 
            'order'             => 'DESC'
        )
    );
?>
<main>
<div class ="style_noticiabox card">
    <h1 class = "styletitlesingle "><?php the_title(); ?></h1>
    <?php  while (have_posts()):
        the_post();
        the_content(); 
    endwhile; ?>
    <div class = "styledate">
       <p>Criado em: <?php echo get_the_date()?> </p>
       <p>Editado em: <?php echo get_the_modified_date()?> </p>
    </div>
</div>

<div class = "containermaisnoticias">
    <h1>Mais notícias: </h1>
    <?php if($news -> have_posts()):
        while($news -> have_posts()):
            $news -> the_post(); ?>
              <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

        <?php endwhile; 
    endif; ?>

</div>
</main>
<?php get_footer(); ?> 

