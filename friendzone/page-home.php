<?php
    //Template Name: Home Page
?>


    <?php get_header()?>
    <section class="introducao">
        <div class="sessao1">
            <h1 class="tema">O PetShop do Seu Melhor Amigo</h1>
            <p>Quem conhece, Confia!</p>
        </div>    
    </section>
</div>
    <section class= "Sobre">
        <a class="Sobresobre" href="#">-SOBRE-</a>
                <p>Somos o PetShop que oferece o melhor serviço da região, somos reconhecidos como o nº 1 em cuidados com o seu animalzinho. Nosso compromisso é ver o nosso cliente sempre feliz!</p>
        <div>
        </div>     
    </section>  
    <section class="section3">
        <a class ="Sobresobre" href="#"><h2>-PRODUTOS-</h2></a>
        <div class="fotos">
            <div class="fotos_linha">
                <a href = "#">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/foto1.jpg">
                    <h3>Tosa</h3>
                </a>    
            </div>
            <div class="fotos_linha">
                <a href = "#">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/foto2.jpg">
                    <h3>Almofada conforto</h3>
                </a>    
            </div>
            <div class="fotos_linha">
                <a href = "#">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/foto3.jpg">
                    <h3>Banquinhos</h3>
                </a>    
            </div>
            <div class="fotos_linha">
                <a href = "#">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/cao e gato.jpg">
                    <h3>Todo conforto</h3>
                </a>
            </div>
    </section>  
    <section>
            <a class ="Sobresobre" href="#"><h2>-NOTÍCIAS-</h2></a>
            <div class="noticias">
                <ul>
                    <li>
                        Mais um ano vencemos o premio Destaque 2020
                    </li>
                    <li>
                        Campanha: Vacine seu animalzinho
                    </li>
                    <li>
                        Próximo sábado, o Petshop Friendzone estará fazendo campanha para castração de cães de rua.
                    </li>
                </ul>
            </div>
    </section>
    <?php get_footer()?>