<?php 
  // Template Name: produto
?>

<?php get_header(); ?>

<div class="card">
  <h1><?php the_title(); ?></h1>
  <?php
  // sempre tem que ter o while para exibir o the_content()
    while (have_posts()):
      the_post();
      the_content();
    endwhile;

    // colcoando 3 produtos aleatorios
    $produtos = new WP_Query(
      array(
        'posts_per_page' => 3,
        'paged' => $paged,
        'post_type' => 'produto',
        'post_status' => 'publish',
        'suppress_filters' => true,
        'orderby' => 'rand',
        'order' => 'ASC'
      )
    );
  ?>
  <div class="links-produtos">

    <?php if($produtos -> have_posts()):
      while($produtos -> have_posts()):
          $produtos -> the_post(); ?>
    
            <!-- titulo com link -->
            <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

      <?php endwhile; 
    endif; ?>

  </div>
</div>

<?php get_footer(); ?>