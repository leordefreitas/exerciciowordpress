<?php
  // Template Name: Produtos
?>

<?php get_header(); 
  // paa criar a paginacao
  $paged = get_query_var('paged', 1);
  $news = new WP_Query(
    array(
      'posts_per_page' => 5,
      'paged' => $paged,
      'post_type' => 'produto',
      'post_status' => 'publish',
      'suppress_filters' => true,
      'orderby' => 'post_date',
      'order' => 'DESC'
    )
  );
?>

<?php if($news -> have_posts()):
  while($news -> have_posts()):
      $news -> the_post(); ?>

      <div class="produtos">
        <!-- titulo com link -->
        <h2 class="titulo-produtos"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
       <!-- descricao -->
        <p class="texto-produtos"><?php echo wp_strip_all_tags(the_content()); ?></p>
      </div>
  <?php endwhile;
else:?>
  <p>Nenhum produto foi encontrado.</p>
<?php endif; ?>

<div class="paginacao">
  <?php
    $big = 999999;
    echo paginate_links(array(
      'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
      'format' => '?paged=%#%',
      'current' => max(1 , get_query_var('paged') ),
      'prev_text' => __('Anterior'),
      'next_text' => __('Proximo'),
      'total' => $news->max_num_pages
    ));
    
  ?>
  
</div>

<?php 
  wp_reset_postdata(); 
?>

<?php get_footer(); ?>